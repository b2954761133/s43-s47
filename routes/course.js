//Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();


//create a course
router.post("/", verify, verifyAdmin, courseController.addCourse)

//route for retrieving all courses
router.get("/all",courseController.getAllCourses);

//create a route for getting all active courses (5 mins.)
// use default endpoint
//getAllActiveCourses
router.get("/",courseController.getAllActiveCourses);

//get a specific course
router.get("/:courseId",courseController.getCourse);

//edit a specific course
router.put("/:courseId",verify, verifyAdmin, courseController.updateCourse);

// Route to search for courses by course name
router.post('/search', courseController.searchCoursesByName);

router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers);

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;