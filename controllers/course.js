const Course = require("../models/Course");
const User = require("../models/User");

//Create a new course
/*
    Steps:
    1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    2. Uses the information from the request body to provide all the necessary information.
    3. Saves the created object to our database and add a successful validation true/false.
*/
module.exports.addCourse = (req, res) => {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newCourse = new Course({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    // Saves the created object to our database
    return newCourse.save().then((course, error) => {

 
        if (error) {
            return res.send(false);

 
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};

//Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//We will use the find() method for our course model

module.exports.getAllCourses = (req,res)=>{
	return Course.find({}).then(result=>{
		return res.send(result)
	})
}

//getAllActiveCourses
//create a function that will handle req,res
//that will find all active courses in the db

module.exports.getAllActiveCourses = (req,res)=>{
	return Course.find({isActive:true}).then(result=>{
		return res.send(result)
	})
}
//Get a specific course
module.exports.getCourse = (req,res)=>{
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result)
	})
}

module.exports.updateCourse = (req,res)=>{

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}


// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


const getEmailsOfEnrolledUsers = async (req, res) => {
	const courseId = req.body.courseId;

	try {
		// Find the course by courseId
		const course = await Course.findById(courseId);

		if (!course) {
			return res.status(404).json({ message: 'Course not found' });
		}

		// Get the userIds of enrolled users from the course
		const userIds = course.enrollees.map(enrollee => enrollee.userId);

		// Find the users with matching userIds
		const enrolledUsers = await User.find({ _id: { $in: users } });

		// Extract the emails from the enrolled users
		const emails = enrolledStudents.forEach(user => user.email);

		res.status(200).json({ userEmails });
	} catch (error) {
		res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
	}
}; 

module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
    const courseId = req.params.courseId; // Use req.params to get the parameter value

    try {
        // Find the course by courseId
        const course = await Course.findById(courseId);

        if (!course) {
            return res.status(404).json({ message: 'Course not found' });
        }

        // Get the userIds of enrolled users from the course
        const userIds = course.enrollees.map(enrollee => enrollee.userId);

        // Find the users with matching userIds
        const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds instead of undefined variable "users"

        // Extract the emails from the enrolled users
        const emails = enrolledUsers.map(user => user.email); // Use map instead of forEach to create an array of emails

        res.status(200).json({ userEmails: emails }); // Use "userEmails" instead of undefined variable "userEmails"
    } catch (error) {
        res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
    }
};
